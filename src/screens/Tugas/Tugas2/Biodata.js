import React, {useEffect} from 'react'
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native'
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import api from '../../../api'

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height

const Biodata = ({ navigation }) => {

    useEffect(() => {
        const getToken = async () => {
            try{
                const token = await AsyncStorage.getItem('token')
                return getVenue(token)
            } catch (error) {
                console.log(error)
            }
        }

        getToken()
    }, [])

    const getVenue = (token) => {
        Axios.get(`${api}/venues`,{
            timeout: 20000, headers: {
                Authorization: 'Bearer' + token,
            },
        })
        .then((res) => {
            console.log(res)
        })
        .catch((error) => {
            console.log(error)
        })
    }

    const onLogoutPress = async () => {
        try {
            await AsyncStorage.removeItem('token')
            navigation.navigate('Login')
        } catch (error) {
            console.log(error)
        }
    }

    return(
        <View style={styles.container}>
          <StatusBar barStyle="dark-content" />
          <View style={styles.photoCard}>
              <Image source={require('../../../assets/images/poto.jpg')} style={styles.profileImage} />
              <Text style={styles.profileText}>Aldi Purnama</Text>
          </View>
          <View style={styles.content}>
              <View style={styles.profileContainer}>
                  <View style={styles.description}>
                      <View style={styles.descriptionTitle}><Text>Tanggal lahir</Text></View>
                      <View style={styles.descriptionText}><Text>15 Maret 1998</Text></View>
                  </View>
                  <View style={styles.description}>
                      <View style={styles.descriptionTitle}><Text>Jenis kelamin</Text></View>
                      <View style={styles.descriptionText}><Text>Pria</Text></View>
                  </View>
                  <View style={styles.description}>
                      <View style={styles.descriptionTitle}><Text>Hobi</Text></View>
                      <View style={styles.descriptionText}><Text>Menulis Puisi</Text></View>
                  </View>
                  <View style={styles.description}>
                      <View style={styles.descriptionTitle}><Text>No. Telp</Text></View>
                      <View style={styles.descriptionText}><Text>082243430883</Text></View>
                  </View>
                  <View style={styles.description}>
                      <View style={styles.descriptionTitle}><Text>Email</Text></View>
                      <View style={styles.descriptionText}><Text>aldipurnama92@yahoo.com</Text></View>
                  </View>
                  <View style={StyleSheet.description}>
                    <TouchableOpacity style={styles.buttonLogout} onPress={() => onLogoutPress()}>
                        <Icon name='logout' size={25} color='#fff'>
                            <Text style={styles.buttonText}>Logout</Text>
                        </Icon>
                    </TouchableOpacity>
                  </View>
              </View>
          </View>
        </View>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.lighter,
  },
  photoCard: {
    height: deviceHeight * 0.3,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3EC6FF'
  },
  content: {
      height: deviceHeight * 0.7
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 150 / 2,
  },
  profileText: {
      color: '#fff',
      marginTop: 10,
      fontSize: 20,
      fontWeight: 'bold',
  },
  profileContainer: {
      flex: 0.5,
      flexDirection: 'column',
      marginHorizontal: 30,
      marginTop: -20,
      elevation: 10,
      borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.8,
      shadowRadius: 2,  
      height: deviceHeight * 0.2,
      backgroundColor: '#fff'
  },
  description: {
      flex: 0.3,
      flexDirection: 'row',
  },
  descriptionTitle: {
      flex: 0.3,
      marginTop: 10,
      marginHorizontal: 10,
      alignItems: 'flex-start'
  },
  descriptionText: {
      flex: 0.7,
      marginTop: 10,
      marginHorizontal: 10,
      alignItems: 'flex-end',
  },
  buttonLogout: {
      backgroundColor: '#3EC6FF',
      marginHorizontal: 10,
      marginVertical: 10,
      alignItems: 'center',
      justifyContent: 'center',
      padding: 5,
      borderRadius: 5,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
    textTransform: 'uppercase'
  },
})
  
export default Biodata;
