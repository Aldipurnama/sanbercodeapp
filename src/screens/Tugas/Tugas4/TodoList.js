import React, {
    useContext,
} from 'react'
import {
    StyleSheet,
    StatusBar,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    FlatList,
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { RootContext } from '../Tugas4'

const TodoList = () => {

    const state = useContext(RootContext)
    
    const renderItem = ({ item }) => {
        return(
            <View style={styles.itemContainer}>
                <View style={{flex: 1}}>
                    <Text>{item.date}</Text>
                    <Text>{item.title}</Text>
                </View>
                <TouchableOpacity onPress={() => state.deleteTodo(item.id)}>
                    <Ionicons name="trash-outline" size={25} />
                </TouchableOpacity>
            </View>
        )
    }

    return(
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <Text style={styles.label}>Masukkan Todolist</Text>
            <View style={styles.inputContainer}>
                <TextInput
                    style={{flex: 1, borderWidth: 2}}
                    value={state.input}
                    placeholder="Input here"
                    onChangeText={(value) => state.handleChangeInput(value)}
                />
                <TouchableOpacity
                    style={styles.buttonAdd}
                    onPress={() => state.addTodo()}>
                    <Ionicons name="add" size={25} />
                </TouchableOpacity>
            </View>
            <FlatList
                style={{flex: 1}}
                data={state.todos}
                renderItem={renderItem}
                keyExtractor={(item) => String(item.id)}
                ItemSeparatorComponent={() => <View style={{height: 12}} />}
            />
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 16,
      backgroundColor: '#fff',
    },
  
    label: {
      width: '100%',
      marginBottom: 5,
    },
  
    inputContainer: {
      width: '100%',
      marginBottom: 24,
      flexDirection: 'row',
    },
  
    buttonAdd: {
      width: 55,
      height: 55,
      marginLeft: 4,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#3ec6ff',
    },
  
    itemContainer: {
      width: '100%',
      padding: 16,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      borderRadius: 4,
      borderWidth: 5,
      borderColor: '#bdc3c7',
    },
  })
  
export default TodoList
