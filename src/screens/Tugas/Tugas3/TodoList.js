import React, {
    useState,
} from 'react'
import {
    StyleSheet,
    StatusBar,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    FlatList,
    Keyboard,
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

const TodoList = () => {
    const [todo, setTodo] = useState('')
    const [data, setData] = useState([])
    const [count, setCount] = useState(0)

    const addItem = (input) => {
        var today = new Date()
        var date = `${today.getDate()}/${today.getMonth()+1}/${today.getFullYear()}`
        const newData = {id: count, tanggal: date, value: input}
        setData([...data, newData])
        setTodo('')
        setCount(count + 1) 
    }

    const deleteItem = (id) => {
        Keyboard.dismiss()
        const filteredData = data.filter((item) => item.id !== id)
        setData(filteredData)
    }

    const TodoListItem = ({id, date, value}) => {
        return(
            <View style={styles.itemContainer}>
                <View style={{flex: 1}}>
                    <Text style={styles.pItem}>{date}</Text>
                    <Text style={styles.pItem}>{value}</Text>
                </View>
                <TouchableOpacity
                    style={styles.iconDelete}
                    onPress={() => deleteItem(id)}>
                    <MaterialCommunityIcons name="trash-can-outline" size={25} />
                </TouchableOpacity>
            </View>
        )
    }

    return(
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <Text style={styles.label}>Masukkan Todolist</Text>
            <View style={styles.inputContainer}>
                <TextInput
                    style={{flex: 1, borderWidth: 2}}
                    value={todo}
                    placeholder="Input here"
                    onChangeText={(input) => setTodo(input)}
                />
                <TouchableOpacity
                    style={styles.buttonAdd}
                    onPress={() => addItem(todo)}>
                    <Ionicons name="add" size={25} />
                </TouchableOpacity>
            </View>
            <FlatList
                style={{flex: 1}}
                data={data}
                keyExtractor={(item) => String(item.id)}
                renderItem={({item}) => (
                    <TodoListItem id={item.id} date={item.tanggal} value={item.value} />
                )}
                ItemSeparatorComponent={() => <View style={{height: 12}} />}
                keyboardShouldPersistTaps="always"
            />
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 16,
      backgroundColor: '#fff',
    },
  
    label: {
      width: '100%',
      marginBottom: 5,
    },
  
    inputContainer: {
      width: '100%',
      marginBottom: 24,
      flexDirection: 'row',
    },
  
    buttonAdd: {
      width: 55,
      height: 55,
      marginLeft: 4,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#3ec6ff',
    },
  
    itemContainer: {
      width: '100%',
      padding: 16,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      borderRadius: 4,
      borderWidth: 5,
      borderColor: '#bdc3c7',
    },
  
    iconDelete: {
      width: 25,
      height: 25,
    },
  })
  
export default TodoList
