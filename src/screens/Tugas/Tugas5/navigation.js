import React, {useState, useEffect} from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Icons from 'react-native-vector-icons/Ionicons'
import SplashScreen from './Splashscreen'
import Intro from './Intro'
import Login from './Login'
import Home from '../Tugas6/Home'
import Profile from '../Tugas2/Biodata'

const Stack = createStackNavigator()
const Tabs = createBottomTabNavigator()

const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Profile" component={TabNavigation} options={{ headerShown: false }} />
    </Stack.Navigator>
)

const TabNavigation = () => {
    return(
        <Tabs.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName

                    if (route.name === 'Home'){
                        iconName = focused ? 'home' : 'home-outline'
                    } else if (route.name === 'Profile'){
                        iconName = focused ? 'person' : 'person-outline'
                    }
                    return <Icons name={iconName} size={size} color={color} />
                }
            })}
            tabBarOptions={{
                style: {
                    backgroundColor: '#fff',
                },
                labelStyle: {
                    fontSize: 12,
                    marginBottom: 5,
                    fontWeight: 'bold',
                },
            }}
        >
            <Tabs.Screen name="Home" component={Home}/>
            <Tabs.Screen name="Profile" component={Profile}/>
        </Tabs.Navigator>
    )
}

const AppNavigation = () => {
    const [ isLoading, setIsLoading ] = useState(true)

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)
    }, [])

    if(isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation
