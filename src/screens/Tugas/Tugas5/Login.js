import React, { useState } from 'react'
import {
    View,
    Text,
    Image,
    TextInput,
    StatusBar,
    TouchableOpacity
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { stylesLogin as styles } from '../../../style/styles'
import api from '../../../api'

const Login = ({ navigation }) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const saveToken = async (token) => {
        try{
            await AsyncStorage.setItem('token', token)
        } catch (e) {
            console.log(e)
        }
    }

    const onLoginPress = () => {
        let data = {
            email: email,
            password: password,
        }
        Axios.post(`${api}/login`, data, {timeout: 20000})
            .then((res) => {
                saveToken(res.data.token)
                navigation.navigate('Profile')
            })
            .catch((error) => {
                alert("Login Failed")
                console.log('Login -> error', error)
            })
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <View style={styles.logoContainer}>
                <Image source={require('../../../assets/images/logo.jpg')} />
            </View>
            <View style={styles.formContainer}>
                <View style={styles.inputContainer}>
                    <Text>Username</Text>
                    <TextInput
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                        style={styles.input}
                        placeholder="Username/ Password"
                    />
                </View>
                <View style={styles.inputContainer}>
                    <Text>Password</Text>
                    <TextInput
                        secureTextEntry={true}
                        value={password}
                        onChangeText={(password) => setPassword(password)}
                        style={styles.input}
                        placeholder="Password"
                    />
                </View>
                <View style={styles.inputContainer}>
                    <TouchableOpacity style={styles.buttonLogin} onPress={() => onLoginPress()}>
                        <MaterialCommunityIcons name='login' size={20} color='#fff'>
                            <Text style={styles.buttonText}>Login</Text>
                        </MaterialCommunityIcons>
                    </TouchableOpacity>
                </View>
                <View style={styles.formDecoration}>
                    <View style={styles.formDecoration2} />
                    <View>
                        <Text style={styles.formDecorationText}>OR</Text>
                    </View>
                    <View style={styles.formDecoration2} />
                </View>
                <View style={styles.inputContainer}>
                    <TouchableOpacity style = {styles.buttonLoginGoogle}>
                        <Ionicons name='logo-google' size={20} color='#fff'>
                            <Text style={styles.buttonText}>Login with Google</Text>
                        </Ionicons>
                    </TouchableOpacity>
                </View>
                <View style={styles.footer}>
                    <Text>Belum mempunyai akun ? </Text>
                    <TouchableOpacity>
                        <Text style={{color: '#3EC6FF'}}>Buat Akun</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Login
