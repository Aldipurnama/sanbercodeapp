import React from 'react'
import { StyleSheet } from "react-native"

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    slide: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },
    title: {
        color: '#191970',
        fontWeight: 'bold',
        fontSize: 20,
    },
    image: {
        width: 300,
        height: 300,
        marginTop: 15,
    },
    text: {
        marginTop: 15,
        color: '#000',
        textAlign: 'center',
        fontSize: 16,
    },
    buttonCircle: {
        width: 50,
        height: 50,
        backgroundColor: '#191970',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius : 200 / 2
    }
})

export const stylesLogin = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    logoContainer: {
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    formContainer: {
        flex: 1,
        padding: 20,
        width: '100%',
    },
    inputContainer: {
        marginBottom: 10,
    },
    input: {
        borderBottomColor: '#000', 
        borderBottomWidth: 1,
    },
    formDecoration: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },
    formDecoration2: {
        flex: 1, 
        height: 1, 
        backgroundColor: '#000'
    },
    formDecorationText: {
        width: 50, 
        textAlign: 'center' 
    },
    buttonLogin: {
        backgroundColor: '#191970',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        borderRadius: 5,
    }, 
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 18,
        textTransform: 'uppercase'
    },
    buttonLoginGoogle: {
        backgroundColor: '#fc0303',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        borderRadius: 5,
    }, 
    footer: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: 24,
        marginBottom: 24,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
})

export default styles
