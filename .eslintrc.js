module.exports = {
  root: true,
  extends: ['airbnb', 'prettier'],
  plugins: 'prettier',
};
