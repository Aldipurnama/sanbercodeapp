/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react'

import Intro from './src/screens/Tugas/Tugas1/Intro'
import Biodata from './src/screens/Tugas/Tugas2/Biodata'
import TodoList from './src/screens/Tugas/Tugas3/TodoList'
import Context from './src/screens/Tugas/Tugas4/index'
import Screens from './src/screens/Tugas/Tugas5/navigation'

const App = () => {
  return (
    <Screens />
  );
};

export default App;
